import ActionTypes from '../actions/ActionTypes';
import {fromJS} from 'immutable';

let initialState = fromJS({
  supported: true,
  hasPermission: false
});

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.NOTIFICATION_NOT_SUPPORTED: {
      return state.merge({
        supported: false
      });
    }

    case ActionTypes.NOTIFICATION_PERMISSION_GRANTED: {
      return state.merge({
        hasPermission: true
      });
    }

    case ActionTypes.NOTIFICATION_PERMISSION_DENIED: {
      return state.merge({
        hasPermission: false
      });
    }

    default:
      return state;
  }
};
