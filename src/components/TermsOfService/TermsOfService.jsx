import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {NavigateActions} from '../../actions';
import {GenUtil} from '../../utility';
import {translationObject} from '../../assets/locales/translations';
const translate = GenUtil.translate;

class TermsOfService extends Component {
  renderContent() {
    var i = 0;
    return Object.keys(translationObject.en.terms).map((key) => {
      /*If the key is heading, don't display anything. Otherwise check if there are more than 2 keys in the value object.
      * If there are more than 2 keys, render the level 1 block for heading and content and the level 2 block for the next level key.
      * If there are less than 2 keys, render the level 1 block.
      */
      if(key!=='heading') {
        if(Object.keys(translationObject.en.terms[key]).length > 2) {
          return Object.keys(translationObject.en.terms[key]).map((keyLevel2) => {
            if(keyLevel2 === 'heading') {
              return this.renderBlock(key, i+=2);
            }

            if(keyLevel2 === 'content') {
              return null;
            }

            return this.renderBlockLevel2(key,keyLevel2, i+=2);
          });
        } else {
          return this.renderBlock(key,i+=2);
        }
      }

      return null;
    });
  }

  renderBlock(key, index) {
    return (
      <div key={ index } >
        <div className='terms-subheading'>
          {translate(`terms.${key}.heading`)}
        </div>
        {translate(`terms.${key}.content`) !== 'content' &&
          <div className='terms-content'>
            {translate(`terms.${key}.content`)}
          </div>
        }
      </div>
    );
  }

  renderBlockLevel2(key, keyLevel2, index) {
    return (
      <div key={ index }>
        <div className='terms-subheading__l2'>
          {translate(`terms.${key}.${keyLevel2}.heading`)}
        </div>
        {translate(`terms.${key}.${keyLevel2}.content`)!== 'content' &&
          <div className='terms-content__l2'>
            {translate(`terms.${key}.${keyLevel2}.content`)}
          </div>
        }
      </div>
    );
  }

  render() {
    return (
      <div className='terms'>
        <div className='terms-header'>
          {translate('terms.heading')}
        </div>
        <span className='terms-radial-container'>
          <div className='terms-radial1'/>
          <div className='terms-radial2'/>
        </span>
        {this.renderContent()}
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    navigate: NavigateActions.navigate
  },
  dispatch
);

export default connect(
  null,
  mapDispatchToProps
)(TermsOfService);