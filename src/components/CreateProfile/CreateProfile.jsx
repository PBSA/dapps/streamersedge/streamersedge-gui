import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import styles from './MUI.css';
import {withStyles} from '@material-ui/core/styles';
import UserInfo from './UserInfo';
import AccountConnections from './AccountConnections';
import {ProfileService} from '../../services';
import {ModalActions, NavigateActions, AccountActions} from '../../actions';
import {ModalTypes, RouteConstants} from '../../constants';
import ProfileFooter from './ProfileFooter';
import {
  facebookBox,
  twitchBox,
  youtubeBox,
  facebookIcon,
  twitchIcon,
  youtubeIcon,
  pubgBox,
  pubgIcon
} from '../../assets/images/profile';
import {GenUtil, ValidationUtil} from '../../utility';
import CommonSelecter from '../../selectors/CommonSelector';
const translate = GenUtil.translate;

class CreateProfile extends Component {
  constructor(props) {
    super(props);

    const {twitchUsername, youtubeUsername, facebookUsername, pubgUsername} = props;
    const path = this.props.path;
    let pathAry = path.split('/')[2];

    if (pathAry === undefined) {
      // The user went directly to the /profile route. Ensure they are on step one.
      this.props.navigate(`${RouteConstants.PROFILE}/1`);
    }

    this.state = this.constructState(twitchUsername, youtubeUsername, facebookUsername, pubgUsername, pathAry);
  }

  componentDidMount() {
    ProfileService.getProfile().then((res) => {
      this.props.setAccount(res);
    }).catch((err) => {
      this.show401Error(err);
    });
  }

  componentDidUpdate(prevProps) {
    if (this.props.account !== prevProps.account) {
      const {twitchUsername, youtubeUsername, facebookUsername, pubgUsername} = this.props;
      const path = this.props.path;
      const pathAry = path.split('/')[2];
      this.setState(this.constructState(twitchUsername, youtubeUsername, facebookUsername, pubgUsername, pathAry));
    }

    // url change
    if (this.state.currentStep !== this.props.path.split('/')[2]) {
      this.setState({currentStep: this.props.path.split('/')[2]});
    }
  }

  constructState = (twitchUsername, youtubeUsername, facebookUsername, pubgUsername, pathAry) => {
    return {
      currentStep: pathAry || '1',
      email: this.props.email || '',
      emailValid: true,
      userType: this.props.userType || translate('createProfile.defaultAccountType'),

      connections: {
        social: {//twitch, facebook, youtube
          header: translate('updateProfile.accountConnections.socialHeader'),
          headerLabel: translate('updateProfile.accountConnections.connectionSelect'),
          headerDescription: translate('updateProfile.accountConnections.connectionDescription'),

          connections: [
            {//twitch
              name: 'twitch',
              headerIcon: twitchBox,
              bodyIcon: twitchIcon,
              bodyUsername: twitchUsername
            },
            {//facebook
              name: 'facebook',
              headerIcon: facebookBox,
              bodyIcon: facebookIcon,
              bodyUsername: facebookUsername
            },
            {//youtube
              name: 'google',
              headerIcon: youtubeBox,
              bodyIcon: youtubeIcon,
              bodyUsername: youtubeUsername
            }
          ]
        },
        game: {//fortnite, pubg, league of legends
          header: translate('updateProfile.accountConnections.gameHeader'),
          headerLabel: translate('updateProfile.accountConnections.connectionSelect'),
          headerDescription: translate('updateProfile.accountConnections.connectionDescription'),

          connections: [
            {//pubg
              name: 'pubg',
              headerIcon: pubgBox,
              bodyIcon: pubgIcon,
              bodyUsername: pubgUsername
            }
          ]
        }
      }
    };
  }

  show401Error(err) {
    if(err.status === 401) {
      this.props.setModalType(ModalTypes.ERROR);
      this.props.setModalData({
        headerText: translate('errors.loggedOut')
      });
      this.props.toggleModal();
    }
  }

  handleEmailChange = (email) => {
    const validation = ValidationUtil.seEmail(email).success || this.props.email === email;
    this.setState({email: email, emailValid: validation});
  }

  setStep = (currentStep) => {
    this.props.navigateToCreateProfile(currentStep);
    this.setState({currentStep});
  }

  openLinkAccountModal = (authRoute) => {
    this.props.setModalType(ModalTypes.LINK_ACCOUNT);
    this.props.setModalData(authRoute);
    this.props.toggleModal();
  }

  openUnlinkAccountModal = (authRoute) => {
    this.props.setModalType(ModalTypes.UNLINK_ACCOUNT);
    this.props.setModalData(authRoute);
    this.props.toggleModal();
  }

  closeLinkAccountModal = () => {
    this.props.toggleModal();
  }

  submitStep1 = () => {
    const {email} = this.state;
    let account = {email: email};

    if(email === this.props.email) { //email has not been changed, we can go to step 2
      this.props.navigateToCreateProfile('2');
    } else { //changed email means we cannot go to step 2 until email has been confirmed
      ProfileService.updateProfile(account)
        .then((res) => {
          this.props.setAccount(res);
          this.props.setModalType(ModalTypes.OK);
          this.props.toggleModal();
          this.props.setModalData({headerText: translate('updateProfile.userInfo.confirmEmail'), redirect: '/profile/2'});
        })
        .catch((err) => {
          this.show401Error(err);

          const entries = Object.entries(err.error);
          this.props.setModalType(ModalTypes.ERROR);

          if(entries && entries.length > 0) {
            this.props.setModalData({headerText: translate('updateProfile.userInfo.updateFailed'), subText: `${entries[0][0]}: ${entries[0][1]}`});
          }else {
            this.props.setModalData({headerText: translate('updateProfile.userInfo.updateFailed')});
          }

          this.props.toggleModal();
        });
    }
  }

  render() {
    const {connections, currentStep, emailValid} = this.state;
    return (
      <div className='create-profile__wrapper'>
        <form className='create-profile' onSubmit={ this.handleSubmit }>
          <div className='create-profile__header'>
            { translate('createProfile.header') }
          </div>
          {currentStep === '1' ?
            <UserInfo handleEmailChange={ this.handleEmailChange } email={ this.state.email } handleUserTypeChange={ this.handleUserTypeChange } />
            :
            <AccountConnections
              hasUnlockedChallenges={ this.props.hasUnlockedChallengeCreate }
              connections={ connections }
              openLinkAccountModal={ this.openLinkAccountModal }
              openUnlinkAccountModal={ this.openUnlinkAccountModal }
              closeLinkAccountModal={ this.closeLinkAccountModal }
            />
          }
        </form>
        <ProfileFooter currentStep={ currentStep } setStep={ this.setStep } submitStep1={ this.submitStep1 }
          navigateToDashboard={ this.props.navigateToDashboard } disabled={ !emailValid }/>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleModal: ModalActions.toggleModal,
    setModalType: ModalActions.setModalType,
    setModalData: ModalActions.setModalData,
    setAccount: AccountActions.setAccountAction,
    navigate: NavigateActions.navigate,
    navigateToDashboard: NavigateActions.navigateToDashboard,
    navigateToCreateProfile: NavigateActions.navigateToCreateProfile
  },
  dispatch
);

const mapStateToProps = (state) => {
  const account = CommonSelecter.getCurrentAccount(state);
  return {
    account,
    email: account.get('email'),
    userType: account.get('userType'),
    twitchUsername: account.get('twitchUserName'),
    youtubeUsername: account.get('googleName'),
    facebookUsername: account.get('facebook'),
    pubgUsername: account.get('pubgUsername'),
    twitch: account.get('twitch'),
    youtube: account.get('youtube'),
    path: state.getIn(['router', 'location', 'pathname']),
    hasUnlockedChallengeCreate: CommonSelecter.hasUserUnlockedChallengeCreate(state)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(CreateProfile));
