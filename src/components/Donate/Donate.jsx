import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {ModalActions} from '../../actions';
import {TextField, IconButton, Button} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import {withStyles} from '@material-ui/core/styles';
import donateBtn from '../../assets/images/donate/donateBtn.png';
import {ChallengeService, PeerplaysService} from '../../services';
import InsufficientFunds from './InsufficientFunds';
import Success from './Success';
import {DonateConstants, ModalTypes} from '../../constants';
import {GenUtil} from '../../utility';
import styles from './MUI.css';
import Config from '../../utility/Config';

const translate = GenUtil.translate;

//TODO: Hook up conditional page states once backend API is ready
class Donate extends Component {
  state = {
    page: DonateConstants.DONATE,
    amount: 0,
    loading: false
  };

  handleChange = (event) => {
    const {name, value} = event.target;

    if(name === 'amount') {
      const newValue = value
        .replace(/[^\d.]/g, '')             // numbers and decimals only
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
        .replace(/(\.[\d]{2})./g, '$1');
      this.setState({
        [name]: newValue
      });
    }else {
      this.setState({
        [name]: value
      });
    }
  };

  updatePage = (page) => {
    this.setState({page: page});
  };

  openSuccessModal = () => {
    const modalData = {header: translate('donate.successHeader'), subText: translate('donate.successSubHeader'), challengeId: this.props.donateToChallenge.challengeId};
    this.props.setModalData(modalData);
    this.props.setModalType(ModalTypes.CREATE_CHALLENGE_SUCCESS);
  }

  donate = () => {
    // eslint-disable-next-line eqeqeq
    if(!this.state.amount || this.state.amount == 0 || this.state.amount === '.') {
      this.props.setModalType(ModalTypes.ERROR);
      this.props.setModalData({headerText: translate('donate.error'), subText: translate('donate.errorZero')});
      return;
    }

    this.setState({loading: true});

    const challengeId = this.props.donateToChallenge.challengeId;

    if(this.props.peerplaysAccountName.startsWith('se-')) {
      ChallengeService.donateToChallenge(challengeId, this.state.amount)
        .then(() => {
          this.setState({loading: false});
          this.openSuccessModal();
        })
        .catch((err) => {
          this.setState({loading: false});

          if(typeof(err) === 'string' && err.includes('unauthorized')) {
            this.props.toggleModal();
            this.props.setModalType(ModalTypes.ERROR);
            this.props.setModalData({
              headerText: translate('errors.loggedOut')
            });
            this.props.toggleModal();
            return;
          }

          if(typeof(err) === 'string' && err.includes('banned')) {
            this.props.setModalType(ModalTypes.BAN);
            return;
          }

          this.handleDonateError(err);
        });
    } else {
      this.createTransaction();
    }
  };

  createTransaction = () => {
    const challengeId = this.props.donateToChallenge.challengeId;

    PeerplaysService.createTransaction(this.props.peerplaysAccountName, this.props.peerplaysPassword, Config.escrow, this.state.amount).then((depositOp) => {
      ChallengeService.donateToChallenge(challengeId, this.state.amount, depositOp)
        .then(() => {
          this.setState({loading: false});
          this.openSuccessModal();
        })
        .catch((err) => {
          this.setState({loading: false});

          if(err.status === 401) {
            this.props.toggleModal();
            this.props.setModalType(ModalTypes.ERROR);
            this.props.setModalData({
              headerText: translate('errors.loggedOut')
            });
            this.props.toggleModal();
            return;
          }

          if(err.status === 403) {
            this.props.setModalType(ModalTypes.BAN);
            return;
          }

          this.handleDonateError(err);
        });
    });
  }

  handleDonateError = (err) => {
    //backend returning message.amount
    if (err.amount) {
      this.updatePage(DonateConstants.ERROR_INSUFFICIENT_FUNDS);
    } else {//backend returning err.message
      this.props.setModalType(ModalTypes.ERROR);
      this.props.setModalData({headerText: translate('donate.error'), subText: err.message});
    }
  }

  render() {
    const {classes, donateToChallenge, peerplaysBalance} = this.props;
    let content = (
      <>
        <div className='donate__txt--title'>
          {translate('donate.donateTo')}
          <span className='donate__txt--name'>{donateToChallenge.challenge}</span>
        </div>
        <div className='donate__input'>
          <p className='donate__txt--label'>{translate('donate.usd')}</p>
          <TextField
            name='amount'
            placeholder={ '0' }
            variant='outlined'
            className={ 'donate__textbox' }
            onChange={ this.handleChange }
            value={ this.state.amount }
            InputProps={ {
              classes: {
                input: 'donate__textbox-input',
                notchedOutline: classes.donateBorder
              }
            } }
          />
        </div>
        <div className='donate__balance-container'>
          <span className='donate__balance-container-balance'>{translate('donate.balance')} {peerplaysBalance} {translate('donate.usd')}</span>
          <span className='donate__balance-container-fee'>{translate('donate.fee')} {this.props.transactionFee} {translate('donate.usd')}</span>
        </div>
        <Button onClick={ this.donate } disabled={ this.state.loading }>
          <img
            className='donate__submit'
            src={ donateBtn }
            alt='Submit'
            type='submit'
          />
        </Button>
      </>
    );

    if (this.state.page === DonateConstants.ERROR_INSUFFICIENT_FUNDS) {
      content = <InsufficientFunds updatePage={ this.updatePage } />;
    } else if (this.state.page === DonateConstants.SUCCESS) {
      content = <Success />;
    }

    return (
      <div className='donate'>
        <IconButton
          className='donate__close'
          aria-label='Close'
          onClick={ this.props.toggleModal }
        >
          <CloseIcon />
        </IconButton>
        {content}
      </div>
    );
  }
}

Donate.propsTypes = {
  donateToChallenge: PropTypes.string.isRequired
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleModal: ModalActions.toggleModal,
    setModalData: ModalActions.setModalData,
    setModalType: ModalActions.setModalType

  },
  dispatch
);

const mapStateToProps = (state) => ({
  donateToChallenge: state.getIn(['modal', 'donateToChallenge']),
  peerplaysBalance: state.getIn(['peerplays', 'balance']),
  peerplaysAccountName: state.getIn(['profiles','currentAccount','peerplaysAccountName']),
  peerplaysPassword: state.getIn(['peerplays','password']),
  transactionFee: state.getIn(['peerplays','transferFee'])
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Donate));
