import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import classNames from 'classnames';
import {List, ListItem, Button} from '@material-ui/core';
import ResultForm from './ResultForm';
import {GenUtil} from '../../utility';
import {ChallengeService} from '../../services';
import {ModalActions} from '../../actions';
import styles from './Mui.css';
import resetButton from '../../assets/images/leftmenu/Reset_btn.svg';

const trans = GenUtil.translate;
const liveLabel = 'leftMenu.links.live';
const popularLabel = 'leftMenu.links.popular';
class LeftMenu extends Component {
  state = {
    options: [
      {label: trans(liveLabel), value: 'live', selected: false},
      {label: trans(popularLabel), value: 'popular', selected: false}
    ],
    searchString: '',
    searchResult: [],
    openResult: false,
    challenges: [],
    hasTransition: false,
    hasUpdated: false,
    update: false,
    displayList: false
  };

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    ChallengeService.getChallenges().then((res,err) => {
      if(!err) {
        this.setState({
          challenges: res.sort((a,b)=> {
            return a.id - b.id;
          })
        });
      }
    }).then((res,err)=>{
      if(!err) {
        this.handleSearch();
      }
    });;
  }

  componentDidUpdate(prevProps, prevState) {
    //Initialize the state when left menu is closed
    if (!this.props.open && this.state.openResult) {
      this.setState({
        openResult: false,
        searchString: '',
        searchResult: [],
        options: [
          {label: trans(liveLabel), value: 'live', selected: false},
          {label: trans(popularLabel), value: 'popular', selected: false}
        ],
        hasUpdated: false,
        displayList: false
      });
    }

    if (prevState.openResult !== this.state.openResult) {
      this.setState({
        hasTransition: false
      });
    } else if (prevProps.open !== this.props.open) {
      if(this.props.open) {
        this.getData();
      }

      this.setState({
        hasTransition: true
      });
    }
  }

  toggleOpen = () => {
    this.setState({
      openResult: !this.state.openResult
    });
  }

  handleChange = (e) => {
    this.setState({
      searchString: e.target.value
    });
  }

  handleKeyUp = (e) => {
    if (e.keyCode === 13) {
      this.handleSearch();
    }
  }

  handleSearch = () => {
    const query = this.state.searchString.toLowerCase();
    let result = this.state.challenges;

    // Arrange challenges by popular
    if (this.state.options[1].selected) {
      result = result.sort((a, b) => {
        return b.joinedUsers.reduce((sum,user)=>sum+user['totalDonation'],0) - a.joinedUsers.reduce((sum,user)=>sum+user['totalDonation'],0);
      });
    }

    // Search challenges for live challenges
    if (this.state.options[0].selected) {
      result = result.filter((challenge) => {
        return challenge.status === 'live';
      });
    }

    // Search challenges by search string
    if (query) {
      result = result.filter((challenge) => {
        return this.searchChallenge(query,challenge);
      });
    }

    if (this.state.options[1].selected || this.state.options[0].selected || query) {
      this.setState({
        openResult: true,
        searchResult: result
      });
    }
  }

  searchChallenge(query, challenge) {
    if (challenge.name.toLowerCase().includes(query) || challenge.game.toLowerCase().includes(query)
      || (challenge.user && challenge.user.username.toLowerCase().includes(query))) {
      return true;
    }

    return false;
  }

  handleFilterClicked = () => {
    this.setState({displayList: !this.state.displayList});
  }

  handleReset = () => {
    this.setState({
      options: [
        {label: trans(liveLabel), value: 'live', selected: false},
        {label: trans(popularLabel), value: 'popular', selected: false}
      ],
      hasUpdated: true,
      searchResult: this.state.challenges.sort((a,b)=>{
        return a.id - b.id;
      })
    }, () => {
      this.handleSearch();
    });
  }

  handleClickOption = (option) => {
    switch (option) {
      case 'live': {
        const newOptions = this.state.options;
        newOptions[0].selected = true;
        this.setState({
          options: newOptions,
          hasUpdated: true
        }, () => {
          this.handleSearch();
        });
        break;
      }

      case 'popular': {
        const newOptions = this.state.options;
        newOptions[1].selected = true;
        this.setState({
          options: newOptions,
          hasUpdated: true
        }, () => {
          this.handleSearch();
        });
        break;
      }

      default: break;
    }
  }

  render() {
    const {open, toggleOpen} = this.props;
    const {openResult, displayList} = this.state;

    return (
      <>
        <div className={ classNames('left-menu', {'left-menu__open': open}) }>
          <div className='left-menu-search'>
            <input
              className='left-menu-search__input'
              placeholder={ trans('leftMenu.search.placeholder') }
              value={ this.state.searchString }
              onChange={ this.handleChange }
              onKeyUp={ this.handleKeyUp }
            />
            <div className='left-menu-search__icon-search' onClick={ this.handleSearch }>
              <i className='fas fa-search left-menu-search__icon--color' />
            </div>
          </div>
          <div className='left-menu-filter-text' onClick={ this.handleFilterClicked }>
            {trans('leftMenu.links.filters')} <i className={ classNames('fas fa-chevron-down', {'fas fa-chevron-up': displayList}) } />
          </div>
          {
            displayList ?
              <div>
                <hr/>
                <List className='left-menu-search__options' disablePadding>
                  {this.state.options.map((option, index) => (
                    <ListItem
                      key={ index }
                      className={ classNames('navlink-item', {'navlink-item--active': option.selected}) }
                      onClick={ () => this.handleClickOption(option.value) }
                      disableGutters
                    >
                      {option.label}
                    </ListItem>
                  ))}
                </List>
                <Button
                  className={ (!this.state.options[0].selected && !this.state.options[1].selected) ? 'left-menu-reset-button disabled' : 'left-menu-reset-button' }
                  onClick={ this.handleReset }
                  disabled={ (!this.state.options[0].selected && !this.state.options[1].selected) }>
                  <img src={ resetButton } alt='' className='left-menu-reset-button__img'/>
                </Button>
              </div>
              : null
          }

        </div>
        {openResult && (
          <ResultForm
            challenges={ this.state.searchResult }
            searchString={ this.state.searchString }
            toggleOpen={ this.toggleOpen }
            toggleLeftMenu = { this.props.toggleOpen }
          />
        )}
        <div
          className={ classNames(
            'left-menu-indicator',
            {
              'left-menu-indicator__open': open,
              'left-menu-indicator__open-result': openResult,
              'left-menu-indicator__transition': this.state.hasTransition
            }
          ) }
          onClick={ toggleOpen }
        />
      </>
    );
  }
}

const mapStateToProps = () => ({
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    setModalType: ModalActions.setModalType,
    setModalData: ModalActions.setModalData,
    toggleModal: ModalActions.toggleModal
  },
  dispatch
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(LeftMenu));
