import React, {Component} from 'react';
import {connect} from 'react-redux';
import {NavLink as Link} from 'react-router-dom';
import {List, ListItem, Tooltip} from '@material-ui/core';
import {GenUtil} from '../../utility';
import CommonSelecter from '../../selectors/CommonSelector';

const translate = GenUtil.translate;

class NavLink extends Component {
  render() {
    const {links, hasUnlockedChallengeCreate} = this.props;

    const rendered = () => {
      return links.map((link) => {
        let linkContent = () => {
          if (link.title.toLowerCase().indexOf('create') !== -1 && !hasUnlockedChallengeCreate) {
            return (
              <Tooltip key={ link.title } title={ translate('tooltip.createChallenge') } placement='top'>
                <div className='navlink-item--disabled'>
                  { link.title }
                </div>
              </Tooltip>
            );
          } else if (link.href) {
            return (
              <Link
                key={ link.title }
                className='navlink-item'
                activeClassName='navlink-item--active'
                to={ link.href }
                exact
              >
                { link.title }
              </Link>
            );
          } else if (link.click) {
            return (
              <Link
                key={ link.title }
                className='navlink-item'
                activeClassName='navlink-item--active'
                to='#'
                onClick={ link.click }
              >
                { link.title }
              </Link>
            );
          }
        };

        return (
          <ListItem key={ link.title } disableGutters>
            {linkContent()}
          </ListItem>
        );
      });
    };

    return (
      <>
        <List disablePadding>
          {rendered()}
        </List>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  const hasUnlockedChallengeCreate = CommonSelecter.hasUserUnlockedChallengeCreate(state);

  return {
    hasUnlockedChallengeCreate
  };
};

export default connect(mapStateToProps, null)(NavLink);
