import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Button, Link} from '@material-ui/core';
import {conditionalDonateButton, shareChallengeButton} from '../../assets/images/challengeDetails';
import ChallengeService from '../../services/ChallengeService';
import {ModalActions, NavigateActions} from '../../actions';
import ModalTypes from '../../constants/ModalTypes';
import {profileDefault} from '../../assets/images/avatar';
import {GenUtil} from '../../utility';
import ParticipantDetails from './ParticipantDetails';
import moment from 'moment';

const translate = GenUtil.translate;

const OPERATORS = {'>':'is greater than','<':'is less than','>=':'is greater than or equal to','<=':'is less than or equal to','=':'is equal to'};


class ChallengeDetails extends Component {
  state = {
    challengeId: null,
    challenge: {
      name: '',
      conditions: [
        {
          challengeId: '',
          join: 'END'
        }
      ],
      joinedUsers: [
        {
          totalDonations: 0
        }
      ],
      user: {
        username: '',
        id: 0,
        avatar: ''
      },
      userId: 0,
      status: ''
    },
    currentBounty: 0,
    donationHistory: []
  }

  componentDidMount() {
    const path = this.props.location.pathname;
    const challengeId = path.split('/')[2];

    this.setState({challengeId: challengeId});
    this.fetchAndUpdateChallengeDetails(challengeId);
    this.timer = setInterval(()=> this.fetchAndUpdateChallengeDetails(challengeId), 5000);
  }

  componentDidUpdate() {
    const path = this.props.location.pathname;
    const newChallengeId = path.split('/')[2];

    // eslint-disable-next-line eqeqeq
    if(this.state.challengeId != newChallengeId) {
      this.setState({challengeId: newChallengeId});
      this.fetchAndUpdateChallengeDetails(newChallengeId);
      clearInterval(this.timer);
      this.timer = setInterval(()=> this.fetchAndUpdateChallengeDetails(newChallengeId), 5000);
    }

  }

  componentWillUnmount() {
    clearInterval(this.timer);
    this.timer = null;
  }

  fetchAndUpdateChallengeDetails(challengeId) {
    ChallengeService.getChallengeById(challengeId).then((challenge) => {
      challenge.conditions = challenge.conditions.sort((a,b) => (a.join > b.join) ? 1 : -1);
      const joinedUsers = challenge.joinedUsers;
      const bounty = this.getBounty(joinedUsers);
      const prevDonations = this.getPreviousDonations(joinedUsers);
      this.setState({challenge: challenge, challengeId: challenge.id, currentBounty: bounty, previousDonations: prevDonations});
      ChallengeService.getDonationHistory(challengeId).then((donationHistory) => {
        donationHistory = donationHistory.sort((a,b)=> moment(b.createdAt).diff(moment(a.createdAt)));
        this.setState({donationHistory});
      }).catch((err)=> console.log(err));
    }).catch((err) => {
      console.log(err);
      this.props.navigateToDashboard();
    });
  }

  //sums up total amount of money donated to challenge
  getBounty = (userArray) => {
    let totalBounty=0;
    userArray.forEach((user) =>  totalBounty += user.totalDonation);

    return totalBounty;
  }

  //checks object array to see if logged in user has already donated to the challenge
  getPreviousDonations = (userArray) => {
    let donationSum = 0;

    if (userArray.length > 0) {
      // eslint-disable-next-line array-callback-return
      donationSum = userArray.reduce((sum, user) => {
        //somehow there are donations with undefined users
        if(user.user) {
          if(user.user.username === this.props.username) {
            return sum + user.totalDonation;
          }
        }

        return sum;
      }, 0);
    }

    return donationSum;
  }

  // if(currentValue.user.username === this.props.username) {
  //   prevDonations += currentValue.totalDonation;
  // }
  getStatusColor(status) {
    let statusColorClass = '';

    switch(status) {
      case 'open':
        statusColorClass = '--green';
        break;
      case 'live':
        statusColorClass = '--red';
        break;
      case 'resolved':
        statusColorClass = '--orange';
        break;
      default:
        break;
    }

    return statusColorClass;
  }

  formatDate(startDate) {
    //format date to output in the following format: October 29, 2019, hh:mm
    let date, options, formattedDate;
    date = new Date(startDate);
    options = {year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric'};

    if(this.props.timeFormat === '24h') {
      formattedDate = date.toLocaleDateString('en-ZA', options);
    } else {
      formattedDate = date.toLocaleDateString('en-US', options);
    }

    return formattedDate;
  }

  formatChallengeRules() {
    const rules = this.state.challenge.conditions;

    if(rules.length > 0) {
      return (
        <>
          {rules.map((rule,index) => (
            <div key={ index }>
              <div className='challenge__container-rules__row'>
                <span>
                  { this.getChallengeRule(rule.param) } {OPERATORS[rule.operator]} { rule.param === 'winTime' ? rule.value + 's' : rule.value }
                </span>
              </div>
              {rule.join !== 'END' && <div className='challenge__container-rules__join'>
                { rule.join }
              </div>}
            </div>
          ))}
        </>
      );
    }
  }

  getChallengeRule(rule) {
    let convertedRule='';

    switch(rule) {
      case 'resultPlace':
        convertedRule = 'Result Place';
        break;
      case 'winTime':
        convertedRule = 'Win Time';
        break;
      case 'frags':
        convertedRule = 'Frags';
        break;
      default:
        break;
    }

    return convertedRule;
  }

  isPeerplaysUser = () => {
    if(this.props.peerplaysAccount) {
      if(this.props.peerplaysAccount.startsWith('se')) {
        this.openDonateModal();
      } else {
        this.openPeerplaysAuthentication();
      }
    } else {
      this.props.setModalData({headerText: translate('challengeDetails.errorPeerplaysHeader'),subText: translate('challengeDetails.errorLinkPeerplaysAccount')});
      this.props.setModalType(ModalTypes.ERROR);
      this.props.toggleModal();
    }
  }

  redirectToUserProfile = (userId) => {
    if(!userId) {
      userId=this.state.challenge.userId;
    }

    if(!this.props.isLoggedIn) {
      this.props.setModalType(ModalTypes.ERROR);
      this.props.setModalData({
        headerText: translate('guestUser.header'),
        subText: translate('guestUser.message')
      });
      this.props.toggleModal();
      return;
    }

    this.props.navigate(`/user-profile/${userId}`);
  }

  openPeerplaysAuthentication = () => {
    const modalData = {
      challengeId: this.state.challenge.id,
      challenge: this.state.challenge.name
    };

    this.props.setModalType(ModalTypes.PEERPLAYS_AUTH);
    this.props.setModalData(modalData);
    this.props.toggleModal();
  }

  openDonateModal = () => {
    const userObect = {
      challengeId: this.state.challenge.id,
      challenge: this.state.challenge.name
    };

    this.props.donateToChallenge(userObect);
    this.props.setModalType(ModalTypes.DONATE);
    this.props.toggleModal();
  }

  openShareModal = () => {
    const modalData = {
      header: translate('challengeDetails.share.header'),
      challengeId: this.state.challenge.id
    };

    this.props.setModalType(ModalTypes.SHARE_CHALLENGE);
    this.props.setModalData(modalData);
    this.props.toggleModal();
  }

  renderAvatar = (avatar) => {
    let avatarImg = avatar;

    if(avatar === '') {
      avatarImg = profileDefault;
    }

    return (
      <img className='challenge__container-streamer-avatar' src={ avatarImg } alt={ '' } onClick={ () => this.redirectToUserProfile(this.state.userId) }/>);
  }

  renderStreamUrlOrStartDate = (challenge) => {
    const renderStreamLink=(
      <>
        <span className='challenge__container-rules__header'>{translate('challengeDetails.livestream')} </span>
        <Link className='challenge__container-rules__row' target='_blank' href={ `${challenge.streamLink}` } color='inherit'>{challenge.streamLink}</Link>
      </>
    );
    const renderStartDate=(
      <>
        <span className='challenge__container-rules__header'>{translate('challengeDetails.startDate')} </span>
        <span className='challenge__container-rules__row'>{this.formatDate(challenge.timeToStart)}</span>
      </>
    );

    if (challenge.streamLink) {
      return renderStreamLink;
    } else {
      return renderStartDate;
    }
  }

  render() {
    const {challenge, currentBounty, previousDonations, userId} = this.state;
    return (
      <div className='challenge'>
        <div className='challenge__details'>
          <div className='challenge__details-header'>
            {translate('challengeDetails.header')}
            <span className={ `challenge__container-challenge-status${this.getStatusColor(challenge.status)}` }>
              <span className={ `challenge__container-challenge-dot${this.getStatusColor(challenge.status)}` }></span>{challenge.status}</span>
          </div>

          <span className='challenge__details-radial-container'>
            <div className='challenge__details-radial1'/>
            <div className='challenge__details-radial2'/>
          </span>

          <div className='challenge__container'>
            <div className='challenge__container-challenge'>
              <div className='challenge__container-challenge__display'>
                <span className='challenge__container-challenge-name'>{challenge.name} |</span>
                <span className='challenge__container-challenge-game'>{challenge.game}</span>
              </div>
            </div>
            <div className='challenge__container-streamer'>
              <span className='challenge__container-streamer-header'>{translate('challengeDetails.streamer')}</span>
              <span>{this.renderAvatar(challenge.user.avatar)}</span>
              <span className='challenge__container-streamer-username' onClick={ () => this.redirectToUserProfile(userId) }>{challenge.user.username}</span>
            </div>
            <div className='challenge__container-ending'>
              <span>{this.renderStreamUrlOrStartDate(challenge)}</span>
            </div>
            <div className='challenge__container-donation'>
              <span className='challenge__container-donation-header'>{translate('challengeDetails.previousDonations')}</span>
              <span className='challenge__container-donation-value'>{previousDonations} USD</span>
            </div>
            {challenge.conditions.length > 0 ?
              <div className='challenge__container-rules'>
                <span className='challenge__container-rules__header'> {translate('challengeDetails.rules')} </span>
                <div className='challenge__container-rules__initial'>
                  {this.formatChallengeRules()}
                </div>
              </div>
              : null }

            <div className='challenge__container-bounty'>
              <div className='challenge__container-bounty-value'>
                {currentBounty.toFixed(2)} {translate('challengeDetails.sUSD')}
              </div>
              <div className='challenge__container-bounty-radial'/>
              <div className='challenge__container-bounty-header'>
                {translate('challengeDetails.currentBounty')}
              </div>
            </div>

            <div className='challenge__details-button__container'>
              {this.props.isLoggedIn && (challenge.status === 'open' || challenge.status ==='live') &&
                <Button
                  className='challenge__details-button__container-button'
                  onClick={ this.isPeerplaysUser }
                  disabled={ challenge.status !== 'open' && challenge.status !== 'live' }
                >
                  <img src={ conditionalDonateButton } className='challenge__details-button__container-button-donate' alt='' />
                </Button>
              }
              <Button className='challenge__details-button__container-button' onClick={ this.openShareModal }>
                <img src={ shareChallengeButton } className='challenge__details-button__container-button-share' alt='' />
              </Button>
            </div>
          </div>
        </div>
        <ParticipantDetails joinedUsers={ this.state.donationHistory } timeFormat= { this.props.timeFormat } onAvatarClick= { this.redirectToUserProfile } />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  username: state.getIn(['profiles', 'currentAccount','username']),
  timeFormat: state.getIn(['profiles', 'currentAccount','timeFormat']),
  peerplaysAccount: state.getIn(['profiles', 'currentAccount','peerplaysAccountName']),
  avatar: state.getIn(['profiles', 'currentAccount', 'avatar']),
  isLoggedIn: state.getIn(['profiles', 'isLoggedIn'])
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleModal: ModalActions.toggleModal,
    setModalData: ModalActions.setModalData,
    setModalType: ModalActions.setModalType,
    donateToChallenge: ModalActions.donateToChallenge,
    navigateToDashboard: NavigateActions.navigateToDashboard,
    navigate: NavigateActions.navigate
  },
  dispatch
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChallengeDetails);
