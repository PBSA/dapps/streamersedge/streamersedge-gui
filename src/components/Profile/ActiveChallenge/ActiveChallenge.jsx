import React, {Component} from 'react';
import {GenUtil, ChallengeUtil} from '../../../utility';
import {profileDefault} from '../../../assets/images/avatar';
import ChallengeCard from '../../Dashboard/ChallengeCard';

const translate = GenUtil.translate;

class ActiveChallenge extends Component {

  render() {
    let {challenges} = this.props;
    let challengeCards = null;
    let activeCardCount = 0;

    if (challenges) {
      challengeCards = challenges.map((challenge) => {

        const donationSum = ChallengeUtil.getTotalDonations(challenge);
        let avatar = profileDefault;

        if (challenge.user) {
          avatar = challenge.user.avatar;
        }

        activeCardCount++;

        return (
          <ChallengeCard
            key={ challenge.id }
            id={ challenge.id }
            name={ challenge.name }
            users={ challenge.joinedUsers }
            reward={ donationSum }
            game={ challenge.game }
            avatar={ avatar }
            startTime = { challenge.timeToStart }
            status = { challenge.status }
          />
        );
      });

    }

    return (
<>
      <div className='active-challenge__section'>
        <div className='dashboard__header'>
          <span className='dashboard__header-txt'>
            {translate('userProfile.active')}
            <span className='active-challenge'> {translate('userProfile.challenge')}</span>
          </span>
          <div className='dashboard__header-bar'>
            <div className='dashboard__header-bar--red'/>
          </div>
          <div className='active-challenge__card'>
            <div className='active-challenge__container'>
              {activeCardCount < 1 ? translate('userProfile.noActive') : challengeCards}
            </div>
          </div>
        </div>
      </div>
      </>
    );
  }
}
export default ActiveChallenge;
