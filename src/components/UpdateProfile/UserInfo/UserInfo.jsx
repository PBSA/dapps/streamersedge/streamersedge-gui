import React, {Component} from 'react';
import ProfilePictureUpload from '../../ProfilePictureUpload';
import CustomInput from '../../CustomInput';
import {
  EmailIcon,
  EmailIconActive,
  InvalidIcon
} from '../../../assets/images/signup';
import SaveButton from '../../../assets/images/preferences/Save.png';
import {GenUtil, ValidationUtil} from '../../../utility';

const translate = GenUtil.translate;

class UserInfo extends Component {
  state = {
    isEmailInputClicked: false
  }

  handleEmailChange = (email) => {
    this.setState({isEmailInputClicked: true});
    this.props.handleEmailChange(email);
  }

  handleUserTypeChange = (value) => {
    this.props.handleUserTypeChange(value);
  }

  render() {
    return(
      <div>
        <span className='update-user-info__header'>
          {translate('updateProfile.userInfo.updateHeader')}
        </span>
        {
          !this.props.hasUnlockedChallenges
            ? <p className='link-accounts-subheading'>{ translate('updateProfile.accountConnections.connectPlus') }</p>
            : null
        }
        <div className='update-user-info__content'>
          <div className='update-user-info__inputs'>
            <div className='update-user-info__email'>
              <span className='update-user-info__email-label'>{translate('updateProfile.userInfo.editEmail')}</span>
              <div className='update-user-info__email-input'>
                <CustomInput
                  name='email'
                  hasActiveGlow={ true }
                  value={ this.props.email }
                  theme='update-profile'
                  placeholder={ translate('register.enterEmail') }
                  handleChange={ this.handleEmailChange }
                  iconLeft={ EmailIcon }
                  iconLeftActive={ EmailIconActive }
                  iconRightActive={ InvalidIcon }
                  handleRightIconClick={ () => {
                    return ValidationUtil.seEmail(this.props.email).errors;
                  } }
                  isValid={ () => {
                    if (this.state.isEmailInputClicked) {
                      return ValidationUtil.seEmail(this.props.email).success;
                    } else {
                      return true;
                    }
                  } }/>
              </div>
            </div>
            <img src={ SaveButton } alt='save button' className='update-profile__save-btn' onClick={ this.props.submit }/>
          </div>
          <div className='update-user-info__avatar'>
            <ProfilePictureUpload />
          </div>
        </div>
      </div>
    );
  }
}

export default UserInfo;
