import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Button, IconButton} from '@material-ui/core';
import CustomInput from '../../CustomInput';
import {AccountActions, ModalActions, PeerplaysActions} from '../../../actions';
import ModalTypes from '../../../constants/ModalTypes';
import {IconPassword, IconPasswordActive, UserIcon, UserIconActive} from '../../../assets/images/login';
import LoginButton from '../../../assets/images/peerplays/login.svg';
import {GenUtil} from '../../../utility';
import {AuthService, PeerplaysService, TransactionService} from '../../../services';
import CloseIcon from '@material-ui/icons/Close';
import Config from '../../../utility/Config';

const translate = GenUtil.translate;

class PeerplausAuth extends Component {
  constructor(props) {
    super(props);
    const {peerplaysUsername} = props;
    this.state = {
      password: '',
      error: '',
      loading: false,
      peerplaysUsername: peerplaysUsername
    };
  }

  handlePasswordChange = (value) => {
    this.setState({password: value});
  }

  handleUsernameChange = (value) => {
    this.setState({peerplaysUsername: value});
  }

  handleSave = () => {
    if(this.state.password.length < 12) {
      this.setState({error: translate('peerplays.passwordLengthError')});
      return;
    }

    this.setState({loading: true});
    const peerplaysAccount = {
      login: this.state.peerplaysUsername,
      password: this.state.password
    };

    AuthService.peerplaysLogin(peerplaysAccount).then((account) => {
      if(this.props.modalData === 'update') {
        this.props.setAccount(account);
        this.props.setModalData('');
        this.props.toggleModal();
      } else if(this.props.modalData.challengeId) {
        this.props.setPeerplaysPassword(this.state.password);
        this.openDonateModal();
      }else {
        PeerplaysService.createTransaction(this.props.modalData.peerplaysAccountName, this.state.password, Config.payment, this.props.modalData.ppyAmount).then((redeemOp) => {
          TransactionService.redeem(null, redeemOp).then(() => {
            this.handleRedeemSuccess();
          }).catch((e) => {
            this.handleRedeemError(e);
          });
        });
      }
    }).catch((err) => {
      this.setState({loading: false});
      this.setState({error: err});
    });
  }

  handleRedeemSuccess = () => {
    this.props.toggleModal();
    this.props.setModalType(ModalTypes.OK);
    this.props.setModalData({
      subText: translate('redeem.success')
    });
    this.props.toggleModal();
  };

  handleRedeemError = (err) => {

    this.props.toggleModal();
    this.props.setModalType(ModalTypes.ERROR);

    if(err.status === 401) {
      this.props.setModalData({
        headerText: translate('errors.loggedOut')
      });
      this.props.toggleModal();
      return;
    }

    this.props.setModalData({
      headerText: translate('redeem.error'),
      subText: ''
    });
    this.props.toggleModal();
  };

  openDonateModal = () => {
    const userObect = {
      challengeId: this.props.modalData.challengeId,
      challenge: this.props.modalData.challenge
    };

    this.props.donateToChallenge(userObect);
    this.props.setModalType(ModalTypes.DONATE);
  }

  render() {
    return (
      <>
        <div className='peerplays-auth-wrapper'>
          <IconButton
            className='donate__close'
            aria-label='Close'
            onClick={ this.props.toggleModal }
          >
            <CloseIcon />
          </IconButton>
          <p className='peerplays-auth-header'>{translate('peerplays.authenticate')}</p>
          <div>
            <div className='peerplays-auth-form__username'>
              <span className='peerplays-auth-form__label'>{translate('peerplays.username')}</span>
              <CustomInput
                name='username'
                muiInputClass='inputRegister'
                iconLeft={ UserIcon }
                iconLeftActive={ UserIconActive }
                hasActiveGlow={ true }
                disabled={ this.props.modalData !== 'update' }
                handleChange={ this.handleUsernameChange }
                placeholder={ 'Username' }
                value={ this.state.peerplaysUsername }
              />
            </div>
            <div className='peerplays-auth-form__password'>
              <span className='peerplays-auth-form__label'>{translate('peerplays.password')}</span>
              <CustomInput
                name='password'
                type='password'
                muiInputClass='inputRegister'
                iconLeft={ IconPassword }
                iconLeftActive={ IconPasswordActive }
                hasActiveGlow={ true }
                placeholder={ 'Password' }
                handleChange={ this.handlePasswordChange }
                value={ this.state.password }
              />
            </div>
          </div>
          <div className='peerplays-auth-form__err'>
            <span>{this.state.error}</span>
          </div>
          <Button disabled={ this.state.loading } className='peerplays-auth-form__submit' onClick={ this.handleSave }>
            <img className='peerplays-auth-form_button' src={ LoginButton } alt=''/>
          </Button>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    peerplaysUsername: state.getIn(['profiles', 'currentAccount', 'peerplaysAccountName']),
    account: state.getIn(['profiles', 'currentAccount']),
    modalData: state.getIn(['modal', 'data'])
  };
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleModal: ModalActions.toggleModal,
    setModalType: ModalActions.setModalType,
    setModalData: ModalActions.setModalData,
    donateToChallenge: ModalActions.donateToChallenge,
    setAccount: AccountActions.setAccountAction,
    setPeerplaysPassword: PeerplaysActions.setPeerplaysPassword
  },
  dispatch
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PeerplausAuth);
