import React, {Component} from 'react';
import {linkAccountRobot, linkAccountButton} from '../../../assets/images/modals';
import CloseIcon from '@material-ui/icons/Close';
import {IconButton} from '@material-ui/core';
import {AccountActions, ModalActions, NavigateActions} from '../../../actions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {GenUtil} from '../../../utility';
import {AuthUtil} from '../../../utility';
import {withRouter} from 'react-router';
import {AuthService} from '../../../services';
import {ProfileService} from '../../../services';
import CustomInput from '../../CustomInput';
import ModalTypes from '../../../constants/ModalTypes';

const translate = GenUtil.translate;

class LinkAccountModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      errText: '',
      pubgUsername: ''
    };
  }

  handleSubmit = () => {
    if(this.props.modalData === 'peerplays') {
      AuthService.linkPeerplaysAccount(this.props.peerplaysAccountName).then((account) => {
        this.props.setAccount(account);
      });
      this.handleClose();
    } else if(this.props.modalData === 'pubg') {
      if(this.state.pubgUsername.length > 3) {
        this.setState({errText:''});
        ProfileService.updateProfile({pubgUsername: this.state.pubgUsername}).then((account) => {
          this.props.setAccount(account);
        }).catch((e)=> {
          if(e.status === 401) {
            this.props.toggleModal();
            this.props.setModalType(ModalTypes.ERROR);
            this.props.setModalData({
              headerText: translate('errors.loggedOut')
            });
          }else if(e.error.pubgUsername){
            this.props.setModalType(ModalTypes.ERROR);
            this.props.setModalData({
              headerText: translate('link.pubgLinkFailed'),
              subText: translate('link.pubgLinkFailedText')
            });
            this.props.toggleModal();
          }
        });
        this.handleClose();
      } else {
        this.setState({errText:translate('link.pubgError')});
      }
    } else {
      AuthUtil.authVia(this.props.modalData, this.props.location.pathname);
      this.handleClose();
    }
  }

  handleTermsClicked = () => {
    this.props.navigateToTermsAndConditions();
    this.props.toggleModal();
  }

  handleClose = () => {
    this.setState({
      errText: '',
      pubgUsername: ''
    });
    this.props.setModalData('');
    this.props.toggleModal();
  }

  handlePubgUsernameChange = (pubgUsername) => {
    this.setState({
      pubgUsername:pubgUsername
    });
  }

  handleKeyPress = (e) => {
    if(e.key === 'Enter') {
      this.handleSubmit();
    }
  };


  render() {
    return (
      <div className='link-account__wrapper'>
        <div className='link-account'>
          <IconButton className='link-account__cross' aria-label='Close' onClick={ this.handleClose }>
            <CloseIcon />
          </IconButton>
          <div className='link-account__icon'>
            <img className='link-account__icon-img' src={ linkAccountRobot } alt=''/>
          </div>
          {this.props.modalData === 'pubg' ?
            <div className='link-account-input' onKeyPress={ (e) => this.handleKeyPress(e) }>
              <CustomInput
                name='pubg'
                hasActiveGlow={ true }
                theme='update-profile'
                handleChange={ this.handlePubgUsernameChange }
                placeholder={ translate('link.pubgNickname') } />
              {this.state.errText!=='' ?
                <div className='link-account-error'>
                  {this.state.errText}
                </div>
                : null}
            </div>
            : null}
          <div className='link-account-text'>
            <p className='link-account-text__header'>{translate('link.header')}</p>
          </div>
          <img onClick={ this.handleSubmit } className='link-account-button' src={ linkAccountButton } alt=''/>
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <span onClick={ this.handleTermsClicked } className='link-account-text__terms' href=''><p >{translate('link.terms')}</p></span>
        </div>
      </div>
    );
  }
}


const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    setModalType: ModalActions.setModalType,
    toggleModal: ModalActions.toggleModal,
    setModalData: ModalActions.setModalData,
    setAccount: AccountActions.setAccountAction,
    navigateToUpdateProfile: NavigateActions.navigateToUpdateProfile,
    navigateToTermsAndConditions: NavigateActions.navigateToTermsAndConditions
  },
  dispatch
);

const mapStateToProps = (state) => ({
  modalData: state.getIn(['modal', 'data']),
  peerplaysAccountName: state.getIn(['profiles','currentAccount','peerplaysAccountName'])
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(LinkAccountModal));
