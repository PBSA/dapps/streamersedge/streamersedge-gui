import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Button, FormControl, FormControlLabel, RadioGroup} from '@material-ui/core';
import NotificationsForm from './NotificationsForm';
import SwitchToggle from '../SwitchToggle';
import BellIcon from '../../assets/images/preferences/Bell.png';
import SettingIcon from '../../assets/images/preferences/Setting_Icon.png';
import preferences from '../../assets/images/preferences/Settings.png';
import {ModalTypes} from '../../constants';
import {GenUtil} from '../../../src/utility';
import {AuthService, UserService, ProfileService} from '../../services';
import {AccountActions, ModalActions, NavigateActions} from '../../actions';
import CustomRadioButton from './CustomRadioButton';

const translate = GenUtil.translate;
const INVITATIONS = ['all', 'users', 'games', 'none'];

class Preferences extends Component {
  state = {
    inviteType: INVITATIONS.indexOf(this.props.invitations).toString(),
    userWhiteList: [],
    userList: [],
    gameList: ['fortnite', 'pubg'],
    gameWhiteList: [],
    notificationType: this.props.notifications ? '1' : '2',
    timeFormat: '12h',
    errors: {
      search: null,
      save: null
    }
  };

  componentDidMount = () => {
    AuthService.getUserList().then((res) => {
      this.setState({userList: res});
    }).catch((err) => {
      if(err.includes('401')) {
        this.props.setModalType(ModalTypes.ERROR);
        this.props.setModalData({
          headerText: translate('errors.loggedOut')
        });
        this.props.toggleModal();
      }
    });

    let DBTimeFormat = this.props.timeFormat;
    this.setState({timeFormat: DBTimeFormat});
  };

  setError = (name, err) => {
    this.setState({
      errors: {
        ...this.state.errors,
        [name]: err
      }
    });
  };

  handleChange = (event) => {
    const {name, value} = event.target;
    this.setState({
      [name]: value
    });
  };

  timeFormatChangeHandler = (name) => (event) => { // eslint-disable-line
    // If is checked, use 24 hour time.
    const timeFormat = event.target.checked ? '24h' : '12h';
    this.setState({timeFormat: timeFormat});
  };

  handleSave = () => {
    // Update notification
    UserService.updateNotification((this.state.notificationType || '1') === '1')
      .then(() => {
        // Update time format
        let account = {timeFormat: this.state.timeFormat};

        ProfileService.updateProfile(account)
        // UserService.updateTimeFormat(this.state.timeFormat)
          .then(() => {
            // Refresh the users data
            ProfileService.getProfile()
              .then((profile) => {
                this.props.setAccount(profile);
                this.props.setModalType(ModalTypes.SUBMIT);
                this.props.toggleModal();
                this.props.setModalData({
                  headerText: translate('preferences.modal.successHeader'),
                  subText: translate('preferences.modal.redirectToDashboard'),
                  redirect: '/dashboard'
                });
              })
              .catch((err) => {
                console.error('Get profile failed', err);

                if(err.status === 401) {
                  this.props.setModalType(ModalTypes.ERROR);
                  this.props.setModalData({
                    headerText: translate('errors.loggedOut')
                  });
                  this.props.toggleModal();
                }
              });
          })
          .catch((err) =>{
            console.error('Update time format failed', err);

            if(err.status === 401) {
              this.props.setModalType(ModalTypes.ERROR);
              this.props.setModalData({
                headerText: translate('errors.loggedOut')
              });
              this.props.toggleModal();
            }
          });
      })
      .catch((err) => {
        console.error('Update notification failed', err);
        this.props.setModalType(ModalTypes.ERROR);

        if(err.status === 401) {
          this.props.setModalData({
            headerText: translate('errors.loggedOut')
          });
          this.props.toggleModal();
          return;
        }

        this.props.setModalData({
          headerText: translate('preferences.modal.errorHeader'),
          subText: translate('preferences.modal.errorSubText')
        });
        this.props.toggleModal();
      });
  };

  addUser = (user) => {
    this.setState({userWhiteList: [...this.state.userWhiteList, user]});
  };

  removeUser = (user) => {
    this.setState({
      userWhiteList: [
        ...this.state.userWhiteList.slice(0, user),
        ...this.state.userWhiteList.slice(user + 1)
      ]
    });
  };

  addGame = (game) => {
    this.setState({gameWhiteList: [...this.state.gameWhiteList, game]});
  };

  removeGame = (game) => {
    this.setState({
      gameWhiteList: [
        ...this.state.gameWhiteList.slice(0, game),
        ...this.state.gameWhiteList.slice(game + 1)
      ]
    });
  };

  render() {
    const {notificationType, timeFormat} = this.state;
    // If 12, the switch should be in the `off` state.
    const initialState = timeFormat === '12h' ? false : true;
    const timeSwitchLabel = {on: translate('preferences.time.twenty4')};

    return (
      <div className='preferences'>
        <div className='preferences__header'>
          <img className='preferences__image' src={ preferences } alt='' />
          <p className='preferences__header-text'> {translate('preferences.header')}</p>
        </div>
        <NotificationsForm
          notificationText={ translate('preferences.notifications.generalHeader') }
          notificationIcon={ SettingIcon }
        >
          <SwitchToggle
            changeHandler={ this.timeFormatChangeHandler }
            initialState={ initialState }
            labels={ timeSwitchLabel }
            switchClass='preferences__time'
          />
        </NotificationsForm>

        <NotificationsForm
          notificationText={ translate('preferences.notifications.header') }
          notificationIcon={ BellIcon }
        >
          <FormControl component='fieldset'>
            <RadioGroup
              aria-label='notification type'
              name='notificationType'
              className={ 'radio-group' }
              value={ notificationType }
              defaultValue='1'
              onChange={ this.handleChange }
            >
              <FormControlLabel classes={ {label: 'label'} } value='1' control={ <CustomRadioButton /> } label={ translate('preferences.notifications.option1') } />
              <FormControlLabel classes={ {label: 'label'} } value='2' control={ <CustomRadioButton /> } label={ translate('preferences.notifications.option2') } />
            </RadioGroup>
          </FormControl>
        </NotificationsForm>
        <div className='form-buttons'>
          <Button className='button-save' onClick={ this.handleSave }>
            {' '}
          </Button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const account = state.getIn(['profiles', 'currentAccount']);
  let timeFormat;

  if (account) {
    timeFormat = account.timeFormat || account.get('timeFormat');
  }

  return {
    notifications: state.getIn(['profiles', 'currentAccount', 'notifications']),
    invitations: state.getIn(['profiles', 'currentAccount', 'invitations']),
    account,
    timeFormat
  };
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    setAccount: AccountActions.setAccountAction,
    toggleModal: ModalActions.toggleModal,
    setModalType: ModalActions.setModalType,
    setModalData: ModalActions.setModalData,
    navigateToDashboard: NavigateActions.navigateToDashboard
  },
  dispatch
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Preferences);
