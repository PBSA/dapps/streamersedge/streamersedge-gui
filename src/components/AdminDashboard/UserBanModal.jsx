import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import AdminService from '../../services/AdminService';
import {withStyles} from '@material-ui/core';
import PropTypes from 'prop-types';
import {ModalTypes} from '../../constants';
import {GenUtil} from '../../utility';
import {ModalActions} from '../../actions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Avatar from '../Avatar';


const trans = GenUtil.translate;

const getModalStyle = () => {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`
  };
};

const styles = (theme) => ({
  button: {
    color:'#599EE4',
    fontSize:'1.8rem',
    [theme.breakpoints.down(1400)]: {
      fontSize: '1.2rem'
    }
  },
  buttonBackground:{
    backgroundColor: '#E9F2FB',
    marginLeft: '1rem'
  },
  details: {
    display:'flex',
    alignItems:'center',
    paddingTop:'2rem',
    paddingBottom:'1rem'
  },
  left:{
    flex:1,
    display:'flex',
    justifyContent:'center',
    alignItems: 'center',
    '& img':{
      width:'13rem',
      height:'13rem'
    }
  },
  right:{
    flex:3,
    paddingLeft:'1.5rem',
    display:'flex',
    flexDirection:'column',
    justifyContent:'center',
    height:'10rem',
    color:'black'
  },
  modalButtons:{
    display:'flex',
    justifyContent: 'flex-end'
  }
});

class UserBanModal extends Component {
  render() {
    const {modalContent,classes,handleClose,refresh,title}= this.props;
    return (
      <div style={ getModalStyle() } className='primary-modal' >
        <span className='primary-modal__title'>{title}</span>
        <div className={ classes.details }>
          <div className={ classes.left }>
            <Avatar avatar={ modalContent.avatar }/>
          </div>
          <div className={ classes.right }>
            <span className='user-type'> {modalContent.userType} </span>
            <span className='primary-modal-user__name'>
              {modalContent.username}
            </span>
            <span className='primary-modal-user__email'>
              {modalContent.email}
            </span>
          </div>
        </div>
        <div className={ classes.modalButtons }>
          <Button classes={ {
            label: classes.button
          } } onClick={ handleClose }
          disableFocusRipple
          >
            CANCEL
          </Button>
          <Button classes={ {
            label: classes.button,
            root: classes.buttonBackground
          } } onClick={ ()=>{
            title === 'Ban User' ? AdminService.banUser(modalContent.reportedUserId).then(()=>{
              this.props.setModalType(ModalTypes.OK);
              this.props.setModalData({subText: trans('adminPanel.banSuccess')});
              this.props.toggleModal();
              handleClose();
              refresh();
            }).catch((e) => {
              if(e.error.email && e.error.email.toLowerCase().includes('this user is not yet unban')){
                this.props.setModalType(ModalTypes.ERROR);
                this.props.setModalData({subText: trans('adminPanel.bannedAlready')});
                this.props.toggleModal();
                handleClose();
                refresh();
              } else {
                this.props.setModalType(ModalTypes.ERROR);
                this.props.setModalData({subText: trans('adminPanel.banFailure')});
                this.props.toggleModal();
                handleClose();
                refresh();
              }
            }) : AdminService.unBanUser(modalContent.reportedUserId).then(()=>{
              this.props.setModalType(ModalTypes.OK);
              this.props.setModalData({subText: trans('adminPanel.unbanSuccess')});
              this.props.toggleModal();
              handleClose();
              refresh();
            }).catch(() => {
              this.props.setModalType(ModalTypes.ERROR);
              this.props.setModalData({subText: trans('adminPanel.unbanFailure')});
              this.props.toggleModal();
              handleClose();
              refresh();
            });
          } }>
            {title.toUpperCase()}
          </Button>
        </div>
      </div>
    );
  }
}

UserBanModal.propTypes = {
  classes: PropTypes.object.isRequired,
  modalContent: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  refresh: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleModal: ModalActions.toggleModal,
    setModalType: ModalActions.setModalType,
    setModalData: ModalActions.setModalData
  },
  dispatch
);

export default connect(
  null,
  mapDispatchToProps
)(withStyles(styles)(UserBanModal));
