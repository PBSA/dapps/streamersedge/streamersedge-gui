import axios from 'axios';
import querystring from 'query-string';

import {Config, GenUtil} from '../utility';

const ApiHandler = axios.create({withCredentials: true});

const apiRoot = Config.isDev
  ? Config.devApiRoute
  : Config.prodApiRoute;

/**
 * Handles all server calls related to user preferences.
 *
 * @class PrivateUserService
 */
class PrivateTransactionService {
  /**
   * Redeem money.
   *
   * @returns {Promise} A promise that resolves to a update status.
   * @param {string} ppyAmount - Amount to redeem.
   * @param {object} redeemOp - Redeem operation.
   */
  static redeem(ppyAmount,redeemOp) {
    const query = `${apiRoot}api/v1/redeem`;
    let body;
    return new Promise(async (resolve, reject) => {
      const headers = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      };

      if(ppyAmount) {
        body = {
          ppyAmount: ppyAmount
        };
      }else {
        body = {
          redeemOp: redeemOp
        };
      }

      try {
        const response = await ApiHandler.post(query, querystring.stringify(body), headers);
        return resolve(response.status);
      } catch (err) {
        reject(err.response);
      }
    });
  }
}

/**
 * Handles all server calls related to User.
 *
 * @class UserService
 */
class TransactionService {
  /**
   * Redeem money.
   *
   * @returns {Promise} A promise that resolves to a update status.
   * @param {string} ppyAmount - Amount to redeem.
   * @param {string} redeemOp - Redeem transaction.
   */
  static redeem(ppyAmount,redeemOp) {
    return GenUtil.dummyDataWrapper(PrivateTransactionService.redeem(ppyAmount,redeemOp));
  }

}

export default TransactionService;
