import AuthService from './AuthService';
import ChallengeService from './ChallengeService';
import PeerplaysService from './PeerplaysService';
import ProfileService from './ProfileService';
import UserService from './UserService';
import NotificationService from './NotificationService';
import GameService from './GameService';
import ReportService from './ReportService';
import TransactionService from './TransactionService';
import PaypalService from './PaypalService';

export {
  AuthService,
  ChallengeService,
  PeerplaysService,
  ProfileService,
  UserService,
  NotificationService,
  GameService,
  ReportService,
  TransactionService,
  PaypalService
};
