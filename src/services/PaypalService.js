import axios from 'axios';
import {Config, GenUtil} from '../utility';
import querystring from 'query-string';

const ApiHandler = axios.create({withCredentials: true});

const apiRoot = Config.isDev
  ? Config.devApiRoute
  : Config.prodApiRoute;

class PrivatePaypalService {
  /**
   * Retrieves paypal payment approval url.
   *
   * @param {string} amount - Total payment amount.
   * @param {string} currency - Currency of the payment.
   * @returns {Promise} A promise that resolves to the paypal approval url.
   */
  static createPaypalPayment(amount, currency) {
    const query = `${apiRoot}api/v1/createPaymentUrl`;
    return new Promise(async(resolve, reject) => {
      const headers = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      };

      const body = {
        amount,
        currency
      };

      try {
        const response = await ApiHandler.post(query, querystring.stringify(body), headers);

        if (response.status === 401) {
          return reject(response);
        }

        if (response.status !== 200) {
          return reject(response);
        }

        return resolve(response.data.result);
      }catch(err) {
        reject(err);
      }
    });
  }
}

/**
 * Handles all paypal server calls.
 *
 * @class PaypalService
 */
class PaypalService {

  /**
   * Retrieves paypal payment approval url.
   *
   * @param {string} amount - Total payment amount.
   * @param {string} currency - Currency of the payment.
   * @returns {Promise} A promise that resolves to the paypal approval url.
   */
  static createPaypalPayment(amount, currency) {
    return GenUtil.dummyDataWrapper(PrivatePaypalService.createPaypalPayment(amount, currency));
  }
}

export default PaypalService;