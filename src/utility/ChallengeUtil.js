/**
 * Challenge Utilities.
 *
 */
class ChallengeUtil {
  /**
   * Takes an array of challenges and filters out the empty ones.
   *
   * @static
   * @param {Array} challenges - Array of challenges.
   * @returns {Array}
   * @memberof ChallengeUtil
   */
  static getJoinedChallenges(challenges) {
    return challenges;
  }


  /**
   * Get Total Donations.
   *
   * @static
   * @param {object} challenge - Challenge data retrieved from the backend API.
   * @returns {number}
   * @memberof ChallengeUtil
   */
  static getTotalDonations(challenge) {
    const donationSum = challenge.joinedUsers.reduce((sum, user) => {
      return sum + user.totalDonation;
    }, 0);

    return donationSum;
  }

  /**
   * Takes a raw challenge date and formats it to be used in a card.
   *
   * @static
   * @param {string} challengeDate - Raw challenge date.
   * @returns {string}
   * @memberof ChallengeUtil
   */
  static formatDate(challengeDate) {
    const formattedDate = new Date(challengeDate);
    return `Ending ${formattedDate.toDateString()}`;
  }
}

export default ChallengeUtil;
